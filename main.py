from pwn import * # https://docs.pwntools.com/en/stable/about.html

f = open("serpai_api", "r")
serpai_api = f.read()
carpetaDoc = "docs/"

def descargar(sitio):
    p = log.progress(sitio)
    p.status('Eliminando ficheros anteriores...')
    import os
    import glob
    ficherosEliminar = glob.glob(carpetaDoc+"*")
    for f in ficherosEliminar:
        os.remove(f)
    p.status('Descargando documentos...')
    busqueda = "site:"+sitio+" ext:xlxs | ext:xls | ext:doc | ext:docx | ext:odt | ext:rtf | ext:sxw | ext:psw | ext:ppt | ext:pptx | ext:pps | ext:csv"
    from serpapi import GoogleSearch # https://github.com/serpapi/google-search-results-python
    search = GoogleSearch({
        "q": busqueda, 
        "location": "Spain",
        "api_key": serpai_api,
        "filter": 0,
        "num": 999
    })
    busqueda = search.get_dict()
    try:
        import time
        for res in busqueda["organic_results"]:
            url = res["link"]
            nombre = url.split("/")[-1]
            wget(url, save=carpetaDoc+nombre, timeout=60)
    except KeyError:
        log.warn("No existen resultados")
    p.success('Descarga finalizada')
    
def sacarMetadatos(carpeta):
    p = log.progress(carpeta)
    p.status('Extrayendo metadatos...')
    import exiftool
    ficheros = os.listdir(carpeta)
    ficheros_ruta = []
    for fi in ficheros:
        ficheros_ruta.append(carpeta+fi)
    if len(ficheros_ruta) > 0:
        with exiftool.ExifToolHelper() as et:
            metadata = et.get_metadata(ficheros_ruta)
        autores = []
        software = []
        company = []
        keywords = []
        titulos = []
        for d in metadata:
            try:
                autores.append(d["FlashPix:Author"])
            except KeyError:
                pass
            try:
                software.append(d["FlashPix:Software"])
            except KeyError:
                pass
            try:
                autores.append(d["FlashPix:LastModifiedBy"])
            except KeyError:
                pass
            try:
                company.append(d["FlashPix:Company"])
            except KeyError:
                pass
            try:
                keywords.append(d["FlashPix:Keywords"])
            except KeyError:
                pass
            try:
                titulos.append(d["FlashPix:Title"])
            except KeyError:
                pass
        autores = list(set(autores))
        software = list(set(software))
        company = list(set(company))
        keywords = list(set(keywords))
        titulos = list(set(titulos))
        p.success('Metadatos extraidos')
        return {"autores": autores,"software": software, "company": company, "keywords": keywords, "titulos": titulos }
    else:
        p.success('No existen ficheros')
        return []
        
def buscar(sitio):
    descargar(sitio)
    return sacarMetadatos(carpetaDoc)

def gui():
    from PyQt5 import QtWidgets, uic, QtCore
    from PyQt5.QtWidgets import QTableWidget,QTableWidgetItem, QWidget, QFileDialog, QInputDialog, QMessageBox, QMenu, QTreeWidgetItem
    from PyQt5.QtCore import Qt
    from PyQt5.QtWidgets import QAction

    app = QtWidgets.QApplication([])
    win = uic.loadUi("main.ui")
    win.show()

    def buscarGui():
        win.progressBar.setValue(0)
        url = win.txtUrl.text()
        win.progressBar.setValue(10)
        descargar(url)
        win.progressBar.setValue(50)
        win.treeEsquema.clear()
        global metadatos
        metadatos = sacarMetadatos(carpetaDoc)
        items = []
        todos = QtWidgets.QTreeWidgetItem(["Todos"])
        for m in metadatos:
            todos.addChild(QtWidgets.QTreeWidgetItem([m]))
        win.treeEsquema.addTopLevelItem(todos)
        win.progressBar.setValue(100)
        
    def getDatos():
        selecionado = win.treeEsquema.selectedItems()[0].text(0)
        win.listDatos.clear()
        for m in metadatos:
            if m == selecionado:
                listItems = metadatos[m]
                for it in listItems:
                    if it.strip() != "":
                        win.listDatos.addItem(it)
    
    #Handlers
    win.btnBuscar.clicked.connect(buscarGui)
    win.treeEsquema.clicked.connect(getDatos)

    sys.exit(app.exec())


#sitio = "https://www.pamplona.es/"
#metadatos = buscar(sitio)
#log.info("Datos:")
#print(metadatos)
gui()
